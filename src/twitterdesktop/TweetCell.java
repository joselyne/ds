/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import twitter4j.JSONObject;
import twitter4j.MediaEntity;
import twitter4j.Status;


/**
 *
 * @author JordyVillao
 */
public class TweetCell extends ListCell<Status>{
    final HBox celda = new HBox();
    final VBox column1 = new VBox();
    final VBox column2 = new VBox();
    final TextArea textoTweet = new TextArea();
    final Label infoTweet = new Label();
    final ListView<ImageView> images = new ListView();
    //final FlowPane urlsTweet = new FlowPane(Orientation.VERTICAL);
    final VBox urlsTweet = new VBox();
    final Rectangle fotoUser = new Rectangle(50,50);
    final Label nombreUser = new Label();
    
    TweetCell()
    {
        images.setOrientation(Orientation.HORIZONTAL);
        
        textoTweet.setWrapText(true);
        textoTweet.setPrefWidth(400);
        textoTweet.setPrefHeight(100);
        celda.setStyle("-fx-border-color: black;" + "-fx-border-width: 10;");
        
        column1.getChildren().addAll(nombreUser,fotoUser);
        column1.setSpacing(10);
        column2.getChildren().addAll(textoTweet,infoTweet,images,urlsTweet);
        column2.setSpacing(10);
        
        celda.getChildren().addAll(column1,column2);
        celda.setSpacing(10);
        
    }
    
    @Override
    public void updateItem(Status item, boolean empty)
    {
        super.updateItem(item,empty);
        if(item == null || empty) 
        {
            textoTweet.setText(null);
            infoTweet.setText(null);
            nombreUser.setText(null);
            setGraphic(null);
        }
        else
        {
            //System.out.println("Urls: "+ item.getURLEntities().length);
            
                        
            if(item.getURLEntities().length != 0)
            {
                for(int i = 0; i<item.getURLEntities().length ; i++)
                {
                    Text link = new Text(item.getURLEntities()[i].getURL());
                    //link.setText(item.getURLEntities()[i].getURL());
                    link.setFill(Color.BLUE);
                    link.setUnderline(true);
                    link.setOnMouseClicked(new EventHandler(){
                        @Override
                        public void handle(Event evt)
                        {
                            Stage stage = new Stage();
                            VentanaWebView webView = new VentanaWebView(link.getText());
                            stage.initModality(Modality.NONE);
                            stage.setTitle("Hypervinculo");
                            stage.setScene(new Scene(webView.root,600,600));
                            stage.show();
                        }
                    });
                    urlsTweet.getChildren().add(link);
                }
//                Hyperlink link = new Hyperlink();
//                link.setText(item.getURLEntities()[0].getURL());
//                link.setOnMouseClicked(new EventHandler(){
//                @Override
//                public void handle(Event evt)
//                {
//                    Stage stage = new Stage();
//                    VentanaWebView webView = new VentanaWebView(link.getText());
//                    stage.initModality(Modality.NONE);
//                    stage.setTitle("Hypervinculo");
//                    stage.setScene(new Scene(webView.root,600,600));
//                    stage.show();
//                }
//                });
//                urlsTweet.getChildren().add(link);
            }
            for(MediaEntity e : item.getMediaEntities()){
                if(e.getType().equals("photo")){
                    ImageView image = new ImageView(e.getMediaURL());
                    image.setFitHeight(150);
                    image.setFitWidth(150);
                    images.getItems().add(image);
                }
            }
            textoTweet.setText(item.getText());
            System.out.println(item.getPlace());
            if(item.getPlace()!=null){
                infoTweet.setText("Ubicacion: "+item.getPlace().getFullName()+"\tFavoritos: "+item.getFavoriteCount()
                              +"\tRetweets: "+item.getRetweetCount());
            }else{
                infoTweet.setText("Ubicacion: -\tFavoritos: "+item.getFavoriteCount()
                              +"\tRetweets: "+item.getRetweetCount());
            }
            nombreUser.setText("@"+item.getUser().getScreenName());
            fotoUser.setFill(new ImagePattern(new Image(item.getUser().getProfileImageURL())));
            
            ContextMenu cm = new ContextMenu();
            MenuItem mi = new MenuItem("Guardar tweet");
            mi.setOnAction(e -> {
                TwiitterJavillao.actualUsuario.getTweetsGuardados().add(item);
                TwiitterJavillao.actualUsuario.getStatusGuardados().add(new PseudoStatus(item.getUser().getScreenName(),
                                item.getText(),item.getUser().getLocation(),item.getFavoriteCount(),item.getRetweetCount()));
                TwiitterJavillao.usuarios.remove(TwiitterJavillao.actualUsuario);
                TwiitterJavillao.usuarios.add(TwiitterJavillao.actualUsuario);
            });
            cm.getItems().add(mi);
            setContextMenu(cm);
            
            setGraphic(celda);
        
        }
    }
}
