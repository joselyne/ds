/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import java.io.Serializable;

/**
 *
 * @author CltControl
 */
public class PseudoStatus  implements Serializable {
    private String nameUser;
    private String textTweet;
    private String placeTweet;
    private int favTweet;
    private int retweetsTweet;

    public PseudoStatus(String nameUser, String textTweet, String placeTweet, int favTweet, int retweetsTweet) {
        this.nameUser = nameUser;
        this.textTweet = textTweet;
        this.placeTweet = placeTweet;
        this.favTweet = favTweet;
        this.retweetsTweet = retweetsTweet;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getTextTweet() {
        return textTweet;
    }

    public void setTextTweet(String textTweet) {
        this.textTweet = textTweet;
    }

    public String getPlaceTweet() {
        return placeTweet;
    }

    public void setPlaceTweet(String placeTweet) {
        this.placeTweet = placeTweet;
    }

    public int getFavTweet() {
        return favTweet;
    }

    public void setFavTweet(int favTweet) {
        this.favTweet = favTweet;
    }

    public int getRetweetsTweet() {
        return retweetsTweet;
    }

    public void setRetweetsTweet(int retweetsTweet) {
        this.retweetsTweet = retweetsTweet;
    }
    
    
    
}
