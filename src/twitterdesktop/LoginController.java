 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author JordyVillao
 */
public class LoginController implements Initializable, ControlledScreen {
    
    ScreensController myController;
    
    @FXML private ListView<Usuario> listAccounts;
    @FXML private Button btnUseOtherAccount;
    private EventHandler clickItemEvent;
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        clickItemEvent = new EventHandler<MouseEvent>(){
            @Override
            public void handle(MouseEvent event) {
                myController.setScreen("twitter");
            }
        };
        listAccounts.setCellFactory( p -> new AccountCell(clickItemEvent) );
        for(Usuario u : TwiitterJavillao.usuarios){
            listAccounts.getItems().add(u);
        }
        

    }
    
    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }

    @FXML
    public void openTwitterVerification(ActionEvent evt){
        System.out.println("Hey! Funciona.");
        
//        VentanaWebView webView = new VentanaWebView(VerificationController.urlAuthorization);
//        
//        Stage stage = new Stage();
//        stage.initModality(Modality.NONE);
//        stage.setScene(new Scene(webView.root,600,600));
//        stage.show();
        
        myController.setScreen("verification");
    }
    
//    @FXML 
//    public void openTwitterWithUser(MouseEvent evt){
//        
//        Usuario u = listAccounts.getSelectionModel().getSelectedItem();
//        TwiitterJavillao.actualUsuario = u;
//        System.out.println(u.getUser().getScreenName());
//        myController.setScreen("twitter");
//        
//    }
//    
    
}
