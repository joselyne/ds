/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Ellipse;
import twitter4j.TwitterException;

/**
 *
 * @author JordyVillao
 */
public class AccountCell extends ListCell<Usuario>{
    final HBox root = new HBox();
    final Ellipse photoUser = new Ellipse(40,40);
    final Label nameUser = new Label();
    final EventHandler<MouseEvent> clickEvent;
    
    AccountCell(EventHandler<MouseEvent> clickEvent){
        this.clickEvent = clickEvent;
        root.setSpacing(10);
        root.getChildren().addAll(photoUser,nameUser);
        root.setStyle("-fx-border-color: black;");
        
    }
    
    @Override
    public void updateItem(Usuario user, boolean empty){
        if(user!= null && !empty){
            try {
                photoUser.setFill(new ImagePattern(new Image(user.getUser().getProfileImageURL())));
                nameUser.setText(user.getTwitter().getScreenName());
            } catch (TwitterException ex) {
                nameUser.setText(user.getNameUser());
            } catch (IllegalStateException ex) {
                
            }
            root.setOnMouseClicked(clickEvent);
            root.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>(){
                @Override
                public void handle(MouseEvent evt) {
                    TwiitterJavillao.actualUsuario = user;
                }
            });
            
            ContextMenu cm = new ContextMenu();
            MenuItem mi = new MenuItem("Eliminar cuenta");
            mi.setOnAction(e -> {
                TwiitterJavillao.usuarios.remove(TwiitterJavillao.actualUsuario);
                TwiitterJavillao.saveUsers();
            });
            cm.getItems().add(mi);
            setContextMenu(cm);
            
            setGraphic(root);
        }
        
    }
    
    
}
