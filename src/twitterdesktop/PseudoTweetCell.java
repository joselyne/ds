/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import twitter4j.Status;

/**
 *
 * @author CltControl
 */
public class PseudoTweetCell extends ListCell<PseudoStatus>{
        final HBox celda = new HBox();
        final VBox column1 = new VBox();
        final VBox column2 = new VBox();
        final Label nombreUser = new Label();
        final TextArea textoTweet = new TextArea();
        final Label infoTweet = new Label();
        final Rectangle fotoUser = new Rectangle(50,50);
        
        PseudoTweetCell(){
            textoTweet.setWrapText(true);
            textoTweet.setPrefWidth(400);
            textoTweet.setPrefHeight(100);
            celda.setStyle("-fx-border-color: black;" + "-fx-border-width: 10;");

            column1.getChildren().addAll(nombreUser,fotoUser);

            column1.setSpacing(10);
            column2.getChildren().addAll(textoTweet,infoTweet);
            column2.setSpacing(10);

            celda.getChildren().addAll(column1,column2);
            celda.setSpacing(10);
        }
        @Override
        public void updateItem(PseudoStatus item, boolean empty)
        {
            super.updateItem(item,empty);
            if(item != null){
                textoTweet.setText(item.getTextTweet());
                nombreUser.setText("@"+item.getNameUser());
                infoTweet.setText("Ubicacion: -\tFavoritos: "+item.getFavTweet()
                              +"\tRetweets: "+item.getRetweetsTweet());
                setGraphic(celda);
            }else{
                textoTweet.setText(null);
                nombreUser.setText(null);
                infoTweet.setText(null);
            }
            
                
        }
        
        
        
        
        
}
