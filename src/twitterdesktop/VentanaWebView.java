/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 *
 * @author JordyVillao
 */
public class VentanaWebView {
    VBox root;
    WebView webView;
    
    VentanaWebView(String url)
    {
        webView = new WebView();
        WebEngine engine = webView.getEngine();
        engine.load(url);
        root = new VBox(webView);
        
    }
}
