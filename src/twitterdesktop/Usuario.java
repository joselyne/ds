/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package twitterdesktop;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author JordyVillao
 */
public class Usuario implements Serializable{
    private Twitter twitter;
    private User user;
    private ArrayList<Status> tweetsGuardados;
    private ArrayList<PseudoStatus> statusGuardados;
    private String nameUser;
            
    
    public Usuario(AccessToken accessToken){
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setJSONStoreEnabled(true)
                .setOAuthConsumerKey("RVfjqQJNBL6hwoYr57ubbKK1l")
                .setOAuthConsumerSecret("aloF0zCPIA9vtz7iJnWqJIqDcvOJer2xZ0LxWrFa5Ye7CAI39F");
  
        TwitterFactory factory = new TwitterFactory(cb.build());
        twitter = factory.getInstance();
//        twitter.setOAuthConsumer("RVfjqQJNBL6hwoYr57ubbKK1l","aloF0zCPIA9vtz7iJnWqJIqDcvOJer2xZ0LxWrFa5Ye7CAI39F");
        twitter.setOAuthAccessToken(accessToken);
        try {
            user = twitter.showUser(twitter.getId());
            nameUser = user.getScreenName();
        } catch (TwitterException ex) {
            ex.printStackTrace();
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
        
        tweetsGuardados = new ArrayList();
        statusGuardados = new ArrayList();
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public ArrayList<PseudoStatus> getStatusGuardados() {
        return statusGuardados;
    }

    public void setStatusGuardados(ArrayList<PseudoStatus> statusGuardados) {
        this.statusGuardados = statusGuardados;
    }

    public Twitter getTwitter() {
        return twitter;
    }

    public void setTwitter(Twitter twitter) {
        this.twitter = twitter;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Status> getTweetsGuardados() {
        return tweetsGuardados;
    }

    public void setTweetsGuardados(ArrayList<Status> tweetsGuardados) {
        this.tweetsGuardados = tweetsGuardados;
    }
    
    
    
}
